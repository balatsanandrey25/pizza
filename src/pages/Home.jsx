import { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import qs from 'qs'
import { useNavigate } from 'react-router-dom';

import Categories from '../components/categories';
import Pizza from '../components/Pizza';
import PizzaSkeleton from '../components/Pizza/skeleton';
import Sort, {listSort} from '../components/sort';
import { setFilters } from '../redux/slice/sliceFilter';
import { featchPizzas } from '../redux/slice/sliceGetPizzas';

function Home() {
  const navigation = useNavigate()
  const dispatch = useDispatch()
  const isSearch = useRef(false)
  const isMounted = useRef(false)
  const settingsFilter = useSelector((state) => state.sliceFilter)
  const {list, status} = useSelector((state) => state.sliceGetPizzas)
  const categoryId = settingsFilter.categoryId
  const sortType = settingsFilter.sortType
 
  const UrlListPizzas = 'https://63982a91fe03352a94c6c91c.mockapi.io/items'

  const getPizzas = () =>{
    const sortBy = sortType.sortProperty.replace('-', '')
    const order = sortType.sortProperty.includes('-') ? 'asc': 'desc';
    const path = `?${categoryId > 0 ? `category=${categoryId}`: ''}&sortBy=${sortBy}&order=${order}`
    const totalPath = UrlListPizzas + path   
    dispatch(featchPizzas(totalPath));  
  }
  // парсим из строки параметры
  useEffect(() => {
    if(window.location.search){
      const params = qs.parse(window.location.search.substring(1))      
      const sort =  listSort.find(obj => obj.sortProperty === params.sortProperty)      
      dispatch(setFilters({
        ...params,
        sortProperty: sort
      }))    
      isSearch.current = true 
    }
  }, [])
  // запрашиваем с базы item 
  useEffect(() => {
    if (!isSearch.current) {
      getPizzas()
    }
    isSearch.current= false
  }, [sortType, categoryId])
  // Вписываем в url строку с параметрами
  useEffect(() => {
    if(isMounted.current){
      const queryStr = qs.stringify({
      sortProperty: sortType.sortProperty,
      categoryId: categoryId
    })
    navigation(`?${queryStr}`)}
    isMounted.current = true
  }, [sortType, categoryId])

  return (
    <>
      <div className="content__top">        
        <Categories  />
        <Sort />
      </div>
      <h2 className="content__title">Все пиццы</h2>
      {status === 'error' ? (
        <div>
          <h2>Произошла ошибка</h2>
          <p>
            Не удалось получить пиццы
          </p>
        </div>
      ) : (
        <div className="content__items">
        {status === 'loding'
          ? [...new Array(6)].map((_, i) => <PizzaSkeleton key={i} />)
          : list.map(item=>{
            return(
              <Pizza key={item.id} {...item}/>
            )
          })}
      </div>
      )
      }
      
    </>
  );
}

export default Home;
import NotFound from "../components/notFound";

function Error() {
  return (
    <NotFound />
  );
}

export default Error;
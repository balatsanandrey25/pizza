import ButtonLink from "../../UI/button/buttonLink";

function NotFound() {


  return (
    <div>
      <h1>Ничего не найдено</h1>
      <p>К сожалению данная страница отсутствует</p>
      <ButtonLink title='На главную' path='/' />
    </div>
  );
}

export default NotFound;
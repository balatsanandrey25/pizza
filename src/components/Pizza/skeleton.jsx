import React from "react"
import ContentLoader from "react-content-loader"

const PizzaSkeleton = (props) => (
  <ContentLoader 
    className="pizza-block"
    speed={2}
    width={280}
    height={460}
    viewBox="0 0 280 460"
    backgroundColor="#f3f3f3"
    foregroundColor="#ecebeb"
    {...props}
  >
    <circle cx="130" cy="130" r="130" /> 
    <rect x="0" y="270" rx="5" ry="5" width="280" height="27" /> 
    <rect x="0" y="318" rx="20" ry="20" width="280" height="88" /> 
    <rect x="0" y="420" rx="10" ry="10" width="92" height="27" /> 
    <rect x="24" y="438" rx="0" ry="0" width="13" height="2" /> 
    <rect x="122" y="414" rx="31" ry="31" width="154" height="45" />
  </ContentLoader>
)

export default PizzaSkeleton
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addProduct } from "../../redux/slice/sliceBusket";

function Pizza(props) {
  const dispatch = useDispatch()
  const listOption =  useSelector((state)=> state.sliceBusket.list)
  const {id, imageUrl,title,types,sizes,price,category, rating} = props
  
  const [pizzaCount, setPizzaCount] = useState(0)
  const [activeIndex, setActiveIndex] = useState(0)
  const [activeIndexForTesto, setActiveIndexForTesto] = useState(0)
  const [totalMoney, setTotalMoney] = useState(price)
  const testo = ['тонкое', 'традиционное']
  const additionalPriceListS = [0, 0.2, 0.3]
  const additionalPriceListT = [0, 0.1]
  
  useEffect(()=>{
    const additionalPrice = 1 + additionalPriceListS[activeIndex] + additionalPriceListT[activeIndexForTesto]
    setTotalMoney(Math.floor(price*additionalPrice))
  }, [activeIndex, activeIndexForTesto])

  const addPizzaInBasket = () =>{
    dispatch(addProduct({
      idPerent: id,
      imageUrl,
      title,
      price: totalMoney,
      types: testo[activeIndexForTesto],
      sizes: sizes[activeIndex]
    }))
  }

  return (
    <div className="pizza-block">
      <img
        className="pizza-block__image"
        src={imageUrl}
        alt="Pizza"
      />
      <h4 className="pizza-block__title">{title}</h4>
      <div className="pizza-block__selector">
        <ul>
          {types.map((item, index)=>{
            return(
              <li className={activeIndexForTesto === index ? 'active' : undefined} onClick={()=>{setActiveIndexForTesto(index)}} key={index}>{testo[item]}</li>
            )
          })}
        </ul>
        <ul>
          {sizes.map((item, index)=>{
            return(
              <li className={activeIndex === index ? 'active' : undefined} onClick={()=>{setActiveIndex(index)}} key={index}>{item} см.</li>
            )
          })}
        </ul>
      </div>
      <div className="pizza-block__bottom">
        <div className="pizza-block__price">{totalMoney} ₽</div>
        <button className="button button--outline button--add" onClick={()=>{
          addPizzaInBasket()
          setPizzaCount(pizzaCount+1)
          }}>
          <svg
            width="12"
            height="12"
            viewBox="0 0 12 12"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M10.8 4.8H7.2V1.2C7.2 0.5373 6.6627 0 6 0C5.3373 0 4.8 0.5373 4.8 1.2V4.8H1.2C0.5373 4.8 0 5.3373 0 6C0 6.6627 0.5373 7.2 1.2 7.2H4.8V10.8C4.8 11.4627 5.3373 12 6 12C6.6627 12 7.2 11.4627 7.2 10.8V7.2H10.8C11.4627 7.2 12 6.6627 12 6C12 5.3373 11.4627 4.8 10.8 4.8Z"
              fill="white"
            />
          </svg>
          <span>Добавить</span>
          <i>{pizzaCount}</i>
        </button>
      </div>
    </div>
  )
}

export default Pizza;

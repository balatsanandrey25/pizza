import { useSelector, useDispatch } from 'react-redux'
import { recordId } from '../../redux/slice/sliceFilter'
function Categories() {
  const categoryId = useSelector((state) => state.sliceFilter.categoryId)
  const dispatch = useDispatch()
  const categoriesList = ['Все', 'Мясные', 'Вегетарианская', 'Гриль', 'Острые', 'Закрытые']
    return (
        <div className="categories">
        <ul>
          {categoriesList.map((item, index)=>{
            return(<li key={index} className={categoryId === index ? 'active' : undefined} onClick={()=>{dispatch(recordId(index))}}>{item}</li>)
          })}
        </ul>
      </div>
    )
  }
  
  export default Categories;
  
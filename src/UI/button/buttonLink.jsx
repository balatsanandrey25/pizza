import { Link } from "react-router-dom"

const ButtonLink = (props) => {
  const {title, path} = props   
  return (
    <Link to={path} className="button button--outline button--add" >      
      <span>Вернутся {title}</span>
    </Link>
  )

}

export default ButtonLink


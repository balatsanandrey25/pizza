import { Routes, Route} from "react-router-dom";
import Header from './components/header';
import Basket from "./pages/Basket";
import Error from "./pages/Error";
import Home from './pages/Home';
import './styles/app.scss';

function App() {

  return (
    <div className="wrapper">
      <Header />
      <div className="content">
        <div className="container">
          <Routes>
            <Route path='/' element={<Home />} />
            <Route path='/basket' element={<Basket />} />
            <Route path='*' element={<Error />} />
          </Routes>
        </div>
      </div>
    </div>
  );
}

export default App;
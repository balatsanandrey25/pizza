import { createSlice } from '@reduxjs/toolkit'
import uniqid from 'uniqid'

const initialState = {
  list: [],
  totalCurent: 0,
  totalPrace: 0
}
const willOutgrow = (state) =>{
  state.totalPrace = state.list.reduce((sum, obj) => {
    return obj.price*obj.counter + sum;
  }, 0)
  state.totalCurent = state.list.reduce((sum, obj) => {
    return obj.counter + sum;
  }, 0)
}
export const sliceBusket = createSlice({
  name: 'busket',
  initialState,
  reducers: {
    addCounter: (state, action) => {
      state.list.map(item => {
        if (item.id === action.payload) {
          item.counter++
          willOutgrow(state)
        } 
      })
    },
    removeCounter: (state, action) => {
      const indexList = state.list.findIndex(item => item.id === action.payload)
      const {counter} = state.list[indexList]
      if (counter > 1) {
        state.list.map(item => {
          if (item.id === action.payload) {
            item.counter--
            willOutgrow(state)
          } 
        })
      } else {
        state.list = state.list.filter((_, index) => index !== indexList)
        willOutgrow(state)
      }
      
             
    },
    addProduct: (state, action) => {
      const { idPerent, price } = action.payload
      const Array = state.list
      const elementSearch = Array.find(item => item.idPerent === idPerent)
      const elementSearchForPrice = Array.find(item => item.price === price)
      if (!Array || !elementSearch) {
        state.list.push({ ...action.payload, id: uniqid(), counter: 1 })
        willOutgrow(state)
        // ('Первый раз или новая группа пиццы')
      } else {
        if (elementSearchForPrice) {
          elementSearchForPrice.counter++
          willOutgrow(state)
          // ('Добавили существующую пиццу')
        }
        else {
          state.list.push({ ...action.payload, id: uniqid(), counter: 1 })
          willOutgrow(state)
          // ("Добавляем с другими параметрами существующую пиццу")
        }
      }

    },
    removeProduct: (state, action) => {
      state.list = state.list.filter(item => item.id !== action.payload)
      willOutgrow(state)
    },
    clearProduct: (state) => {
      state.list = []
      state.totalCurent = 0
      state.totalPrace = 0
    }
  },
})

// Action creators are generated for each case reducer function
export const { addProduct, removeProduct, clearProduct, addCounter, removeCounter } = sliceBusket.actions

export default sliceBusket.reducer
import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  categoryId: 0,
  sortType: {name: 'популярности (DESC)', sortProperty: 'rating'}
}

export const sliceFilter = createSlice({
  name: 'filter',
  initialState,
  reducers: {
    recordId: (state, action) => {
      state.categoryId = action.payload
    },
    recordSortType: (state, action) => {
      state.sortType = action.payload
    },   
    setFilters: (state, action) =>{
      state.categoryId = Number(action.payload.categoryId) 
      state.sortType = action.payload.sortProperty
    } 
  },
})

// Action creators are generated for each case reducer function
export const { recordId, recordSortType, setFilters } = sliceFilter.actions

export default sliceFilter.reducer
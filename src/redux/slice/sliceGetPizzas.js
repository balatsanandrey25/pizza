import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import axios from 'axios';

const initialState = {
  list: [], 
  status: 'loding', // loding | success | error
}
export const featchPizzas = createAsyncThunk('pizzas/fetchByList', async (totalPath) => {
  const {data} = await axios.get(totalPath)    
    return data
  }
)
export const sliceGetPizzas = createSlice({
  name: 'pizzas',
  initialState,
  reducers: {
    setList(state, action) {
        state.list = action.payload 
    }
  },
  extraReducers: {
    [featchPizzas.pending]: (state) =>{
      state.status = 'loding'
      state.list = []
    },
    [featchPizzas.fulfilled]: (state, {payload}) =>{
      state.list = payload
      state.status = 'success'
    },
    [featchPizzas.rejected]: (state) =>{
      state.list = []
      state.status = 'error'
    },
  }
})

// Action creators are generated for each case reducer function
export const { setList } = sliceGetPizzas.actions

export default sliceGetPizzas.reducer
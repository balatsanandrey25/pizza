import { configureStore } from '@reduxjs/toolkit'
import sliceFilter from './slice/sliceFilter'
import sliceBusket from './slice/sliceBusket'
import sliceGetPizzas from './slice/sliceGetPizzas'
export const store = configureStore({
  reducer: {
    sliceFilter,
    sliceBusket,
    sliceGetPizzas,
  },
})